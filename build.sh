#!/bin/bash

MJML=node_modules/.bin/mjml


for file in `ls -1 mjml/*.keycloak`
do 
  $MJML ${file%.*}.keycloak --config.filePath ./mjml/partials/ --output src/email/html/`basename ${file%.*}.ftl`
done


