### Yawik Keycloak Theme

A theme for keycloak login pages which follows Yawik project theme. Based on the [keycloak-fairlogin-theme](https://git.fairkom.net/fairlogin/keycloak-fairlogin-theme) 

![Login page english](./login/resources/img/yawik-keycloak-theme-animated.gif)

#### Installation

1. Clone this repository to `themes/` directory of your keycloak installation

    git clone https://gitlab.com/yawik/keycloak-theme.git yawik

2. Open admin console and go to your realm settings. Change login theme with yawik.

**Important Note**: The current version of yawik keycloak-theme integrates only login and email theme. Do not use it for account or admin theme.

Note: You may also want to enable localization. Set `Internationalization Enabled` to on and set your default locale.

#### Development

Start keycloak standalone server and don't forget to disable theme caching.

./standalone/configuration/standalone.xml

    ...
    <theme>
      <staticMaxAge>-1</staticMaxAge>
      <cacheThemes>false</cacheThemes>
      <cacheTemplates>false</cacheTemplates>
      <dir>${jboss.home.dir}/themes</dir>
    </theme>
    ...


#### Email Templates

mail templates are generated using [mjml](https://mjml.io/). Sources are located at: https://gitlab.com/yawik/backend/-/tree/main/mjml